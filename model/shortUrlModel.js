const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const shortUrlSchema = new Schema({
    original_url: { type: String, required: true, unique: true },
    short_url: { type: Number, unique: true}
});

module.exports = mongoose.model('ShortUrl', shortUrlSchema);