const express = require('express');
const router = express.Router();
const shortUrlController = require('../controller/shortUrlController');

router.post('/api/shorturl', shortUrlController.saveShortUrl)
router.get('/api/shorturl/:shorturl', shortUrlController.getShortUrlById)

module.exports = router;