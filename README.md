# URL Shortener Microservice

Certification Project for Backend Development and APIs by FreeCodeCamp
Made with NodeJS
Link to project requirements: [FreeCodeCamp - URL Shortener Tracker](https://www.freecodecamp.org/learn/back-end-development-and-apis/back-end-development-and-apis-projects/url-shortener-microservice).
