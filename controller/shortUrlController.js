const ShortUrl = require('../model/shortUrlModel');
const dns = require('dns');

exports.saveShortUrl = async (req, res) => {

    const { url } = req.body;
    const urlRegex = /^https:\/\/[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}(\/.*)?$/i;
    let response;

    if (!url.match(urlRegex)) {
        return response = res.json({'error': 'invalid url'})
    }
    const randomNumber = Math.floor(Math.random() * 10000);

    const newShortUrl = new ShortUrl({
        original_url: url,
        short_url: randomNumber
    })

    try {
        const savedShortUrl = await newShortUrl.save();
        response = res.json({
            "original_url": savedShortUrl.original_url,
            "short_url": savedShortUrl.short_url
        })
    } catch (e) {
        console.error(e)
    }
}

exports.getShortUrlById = async (req, res) => {
    const shortUrl = req.params.shorturl
    ShortUrl.findOne({short_url: shortUrl}, (err, data) => {
        if (err) return console.error(err);
        const { original_url } = data;
        res.redirect(original_url);

    })
}


