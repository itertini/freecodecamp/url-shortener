require('dotenv').config();
const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const app = express();
const shortUrlRoutes = require('./routes/shortUrlRoutes')

// Basic Configuration
const port = process.env.PORT || 3000;

mongoose.connect(process.env.MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true });
const Schema = mongoose.Schema;

app.use(bodyParser.urlencoded({ extended: "false" }));
app.use(bodyParser.json());

app.use(cors());

app.use('/', shortUrlRoutes)

app.use('/public', express.static(`${process.cwd()}/public`));


//////////////////////


const createAndSaveUrl = (done) => {
  const shortUrlNum = Math.random(Math.random() * 100);
  const ShortUrl = new ShortUrl({
    original_url: req.body,
    short_url: shortUrlNum
  })

  ShortUrl.save((err, data) => {
    if (err) return console.error(err);
    done(null, data)
  })
}

app.get('/', function(req, res) {
  res.sendFile(process.cwd() + '/views/index.html');
});

// Your first API endpoint
// app.get('/api/hello', function(req, res) {
//   res.json({ greeting: 'hello API' });
// });

// app.post('/api/shorturl', (req, res) => {
//   res.json(req.body)
// })

app.listen(port, function() {
  console.log(`Listening on port ${port}`);
});


