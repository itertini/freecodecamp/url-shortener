const dns = require('dns')

function checkDomainExists(domain) {
    return new Promise((resolve, reject) => {
        dns.lookup(domain, (err, address, family) => {
            if (err) {
                resolve(false);        
            } else {
                resolve(true);
            }
        })
    })
}